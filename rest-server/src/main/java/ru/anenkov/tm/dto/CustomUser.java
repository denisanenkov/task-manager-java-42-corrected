package ru.anenkov.tm.dto;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CustomUser extends User {

    private String userId;

    public CustomUser withUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public CustomUser(
            UserDetails user
    ) {
        super(user.getUsername(),
                user.getPassword(),
                user.isEnabled(),
                user.isAccountNonExpired(),
                user.isCredentialsNonExpired(),
                user.isAccountNonLocked(),
                user.getAuthorities());
    }

    public CustomUser(String username,
                      String password,
                      Collection<? extends GrantedAuthority> authorities
    ) {
        super(username, password, authorities);
    }

    public CustomUser(String username,
                      String password,
                      boolean enabled,
                      boolean accountNonExpired,
                      boolean credentialsNonExpired,
                      boolean accountNonLocked,
                      Collection<? extends GrantedAuthority> authorities
    ) {
        super(username, password,
                enabled, accountNonExpired,
                credentialsNonExpired, accountNonLocked, authorities);
    }

}
