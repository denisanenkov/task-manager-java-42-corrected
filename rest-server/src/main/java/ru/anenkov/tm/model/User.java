package ru.anenkov.tm.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "app_user")
public class User {

    @Id
    private String id = UUID.randomUUID().toString();

    @Column(name = "login")
    private String login;

    @Column(name = "passwordHash")
    private String passwordHash;

    @Column(name = "firstName")
    private String firstName = "";

    @Column(name = "secondName")
    private String secondName = "";

    @Column(name = "lastName")
    private String lastName = "";

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Role> roles;

}

