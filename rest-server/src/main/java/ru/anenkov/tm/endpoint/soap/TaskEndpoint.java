package ru.anenkov.tm.endpoint.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.service.TaskService;
import ru.anenkov.tm.dto.TaskDTO;
import ru.anenkov.tm.model.Task;
import ru.anenkov.tm.util.UserUtil;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

@Component
@WebService
public class TaskEndpoint {

    @Autowired
    private TaskService taskService;

    @WebMethod
    public void addDTOTask(
            @Nullable @WebParam(name = "task") final TaskDTO task
    ) {
        taskService.addDTO(task);
    }

    @WebMethod
    public void addTask(
            @Nullable @WebParam(name = "task") final Task task
    ) {
        taskService.add(task);
    }

    @WebMethod
    public void removeAllByUserIdAndProjectId(
            @Nullable @WebParam(name = "projectId") final String projectId
    ) {
        taskService.removeAllByUserIdAndProjectId(UserUtil.getUserId(), projectId);
    }

    @WebMethod
    public List<TaskDTO> findAllByUserIdAndProjectId(
            @Nullable @WebParam(name = "projectId") final String projectId
    ) {
        return TaskDTO.toTaskListDTO(taskService.findAllByUserIdAndProjectId(UserUtil.getUserId(), projectId));
    }

    @WebMethod
    public void removeTaskByUserIdAndProjectIdAndId(
            @Nullable @WebParam(name = "projectId") final String projectId,
            @Nullable @WebParam(name = "id") final String id
    ) {
        taskService.removeTaskByUserIdAndProjectIdAndId(UserUtil.getUserId(), projectId, id);
    }

    @WebMethod
    public TaskDTO findTaskByUserIdAndProjectIdAndId(
            @Nullable @WebParam(name = "projectId") final String projectId,
            @Nullable @WebParam(name = "id") final String id
    ) {
        return TaskDTO.toTaskDTO(taskService.findTaskByUserIdAndProjectIdAndId(UserUtil.getUserId(), projectId, id));
    }

    @WebMethod
    public TaskDTO findTaskByUserIdAndProjectIdAndName(
            @Nullable @WebParam(name = "projectId") final String projectId,
            @Nullable @WebParam(name = "name") final String name
    ) {
        return TaskDTO.toTaskDTO(taskService.findTaskByUserIdAndProjectIdAndId(UserUtil.getUserId(), projectId, name));
    }

    @WebMethod
    public long countByUserIdAndProjectId(
            @Nullable @WebParam(name = "projectId") final String projectId
    ) {
        return taskService.countByUserIdAndProjectId(UserUtil.getUserId(), projectId);
    }

}
