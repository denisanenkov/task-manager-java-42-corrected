package ru.anenkov.tm.endpoint.rest;

import org.springframework.beans.factory.annotation.Autowired;
import ru.anenkov.tm.exception.rest.NoSuchEntitiesException;
import ru.anenkov.tm.api.endpoint.IProjectRestEndpoint;
import org.springframework.web.bind.annotation.*;
import ru.anenkov.tm.service.ProjectService;
import org.springframework.http.MediaType;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.dto.ProjectDTO;
import ru.anenkov.tm.util.UserUtil;

import java.util.List;

@RestController
@RequestMapping("/api")
public class ProjectRestEndpoint implements IProjectRestEndpoint {

    @Autowired
    private ProjectService projectService;

    @Override
    @PostMapping(value = "/project")
    public ProjectDTO add(@Nullable @RequestBody ProjectDTO project) {
        project.setUserId(UserUtil.getUserId());
        projectService.addDTO(project);
        return project;
    }

    @Override
    @PutMapping(value = "/project")
    public ProjectDTO update(@Nullable @RequestBody ProjectDTO project) {
        project.setUserId(UserUtil.getUserId());
        projectService.addDTO(project);
        return project;
    }

    @Override
    @GetMapping(value = "/project/{id}")
    public @Nullable ProjectDTO findOneByIdEntity(@Nullable @PathVariable("id") final String id) {
        return ProjectDTO.toProjectDTO(projectService.findProjectByIdAndUserId(id, UserUtil.getUserId()));
    }

    @Override
    @DeleteMapping(value = "/project/{id}")
    public String removeOneById(@Nullable @PathVariable final String id) {
        ProjectDTO project = ProjectDTO.toProjectDTO
                (projectService.findProjectByIdAndUserId(id, UserUtil.getUserId()));
        if (project == null)
            throw new NoSuchEntitiesException("project with id \"" + id + "\" does not exist!");
        projectService.removeProjectByUserIdAndId(UserUtil.getUserId(), id);
        return "Project with id \"" + id + "\" was deleted successfully!";
    }

    @Override
    @DeleteMapping(value = "/projects")
    public void removeAllProjects() {
        projectService.removeAllByUserId(UserUtil.getUserId());
    }

    @Override
    @Nullable
    @GetMapping(value = "/projects")
    public List<ProjectDTO> getListByUserId() {
        return ProjectDTO.toProjectListDTO(projectService.findAllByUserId(UserUtil.getUserId()));
    }

    @Override
    @GetMapping(value = "/projects/count")
    public long count() {
        return projectService.countByUserId(UserUtil.getUserId());
    }

}
