package ru.anenkov.tm.rest;

import ru.anenkov.tm.communication.ProjectCommunication;
import ru.anenkov.tm.dto.ProjectDTO;
import ru.anenkov.tm.endpoint.soap.ProjectEndpointService;
import ru.anenkov.tm.endpoint.soap.AuthEndpointService;
import ru.anenkov.tm.endpoint.soap.ProjectEndpoint;
import ru.anenkov.tm.endpoint.soap.AuthEndpoint;

import java.util.List;

public class ProjectRestEndpointTest {

    static final AuthEndpointService authEndpointService = new AuthEndpointService();
    static final AuthEndpoint authEndpoint = authEndpointService.getAuthEndpointPort();

    static final ProjectEndpointService projectEndpointService = new ProjectEndpointService();
    static final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    public static void main(String[] args) {
        authEndpoint.login("forTests", "forTests");
        ProjectRestEndpointTest projectRestEndpointTest = new ProjectRestEndpointTest();
        projectRestEndpointTest.findAll();
    }

    public void findAll() {
        ProjectCommunication projectCommunication = new ProjectCommunication();
        List<ProjectDTO> projectDTO = projectCommunication.projects();
        System.out.println(projectDTO);
    }
}
