package ru.anenkov.tm;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.lang.Nullable;
import ru.anenkov.tm.bootstrap.BootstrapClient;
import ru.anenkov.tm.configuration.MyConfiguration;

public class ApplicationClient {

    @SneakyThrows
    public static void main(@Nullable final String[] args) {
        @NotNull final AbstractApplicationContext context = new AnnotationConfigApplicationContext(MyConfiguration.class);
        context.registerShutdownHook();
        context.getBean(BootstrapClient.class).run(args);
    }

}
