package ru.anenkov.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.anenkov.tm.dto.ProjectDTO;
import ru.anenkov.tm.model.Project;

import java.util.List;

@RequestMapping("/api")
public interface IProjectRestEndpoint {

    @Nullable
    @PostMapping("/project")
    ProjectDTO add(@Nullable ProjectDTO project);

    @Nullable
    @PutMapping("/project")
    ProjectDTO update(@Nullable ProjectDTO project);

    @GetMapping("/project/{id}")
    @Nullable ProjectDTO findOneByIdEntity(@PathVariable("id") String id);

    @Nullable
    @DeleteMapping("/project/{id}")
    String removeOneById(@PathVariable String id);

    @DeleteMapping("/projects")
    void removeAllProjects();

    @Nullable
    @GetMapping("/projects")
    List<ProjectDTO> getListByUserId();

    @GetMapping("/projects/count")
    long count();

}
