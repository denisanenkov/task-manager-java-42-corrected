package ru.anenkov.tm.util;

public interface CredentialsUtil {

    static String[] getCredentials() {
        String[] credentials = new String[2];
        System.out.print("LOGIN: ");
        String login = TerminalUtil.nextLine();
        System.out.print("PASSWORD: ");
        String password = TerminalUtil.nextLine();
        credentials[0] = login;
        credentials[1] = password;
        return credentials;
    }

}
