package ru.anenkov.tm.exception;

import org.jetbrains.annotations.NotNull;

public class IncorrectCommandException extends AbstractException {

    public IncorrectCommandException(@NotNull final String command) {
        super("Error! ``" + command + "`` is not a command! Re-enter command!");
    }

    public IncorrectCommandException(Throwable cause) {
        super(cause);
    }

    public IncorrectCommandException() {
    }

}