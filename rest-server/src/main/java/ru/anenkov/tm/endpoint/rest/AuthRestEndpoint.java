package ru.anenkov.tm.endpoint.rest;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.service.UserService;
import ru.anenkov.tm.model.User;
import lombok.SneakyThrows;

import javax.jws.WebMethod;
import javax.jws.WebParam;

@RestController
@RequestMapping("/api")
public class AuthRestEndpoint {

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @WebMethod
    @GetMapping(value = "/login/{username}/{password}")
    public boolean login(
            @Nullable @WebParam(name = "username") final String username,
            @Nullable @WebParam(name = "password") final String password
    ) {
        return userService.login(username, password);
    }

    @WebMethod
    @GetMapping(value = "/profile")
    public User profile() {
        return userService.profile();
    }

    @WebMethod
    @GetMapping(value = "/logout")
    public boolean logout() {
        return userService.logout();
    }

    @Nullable
    @SneakyThrows
    @GetMapping(value = "/admin/getUserByLogin/{login}")
    public User findByLogin(
            @PathVariable @Nullable final String login
    ) {
        return userService.findByLogin(login);
    }

    @Nullable
    @SneakyThrows
    @GetMapping(value = "/admin/getUserByFirstName/{firstName}")
    public User findUserByFirstName(
            @PathVariable @Nullable final String firstName
    ) {
        return userService.findUserByFirstName(firstName);
    }

    @Nullable
    @SneakyThrows
    @GetMapping(value = "/admin/getUserBySecondName/{secondName}")
    public User findUserBySecondName(
            @PathVariable @Nullable final String secondName
    ) {
        return userService.findUserBySecondName(secondName);
    }

    @Nullable
    @SneakyThrows
    @GetMapping(value = "/admin/getUserByLastName/{lastName}")
    public User findUserByLastName(
            @PathVariable @Nullable final String lastName
    ) {
        return userService.findUserByLastName(lastName);
    }

    @SneakyThrows
    @DeleteMapping("/admin/deleteUser/{login}")
    public void deleteUserByLogin(
            @PathVariable @Nullable final String login
    ) {
        userService.deleteUserByLogin(login);
    }

    @Nullable
    @SneakyThrows
    @PostMapping("/saveUser")
    public User save(
            @RequestBody @Nullable final User user
    ) {
        user.setPasswordHash(passwordEncoder.encode(user.getPasswordHash()));
        return userService.save(user);
    }

    @Nullable
    @SneakyThrows
    @PutMapping("/updateUser")
    public User update(
            @RequestBody @Nullable final User user
    ) {
        return userService.save(user);
    }

    @Nullable
    @SneakyThrows
    @GetMapping("/getUserById/{id}")
    public User findById(
            @PathVariable @Nullable final String id
    ) {
        return userService.findById(id);
    }

    @Nullable
    @SneakyThrows
    @GetMapping("/admin/findAll")
    public Iterable<User> findAll() {
        return userService.findAll();
    }

    @SneakyThrows
    @GetMapping("/admin/usersCount")
    public long count() {
        return userService.count();
    }

    @SneakyThrows
    @DeleteMapping("/admin/deleteById/{id}")
    public void deleteById(
            @PathVariable @Nullable final String id
    ) {
        userService.deleteById(id);
    }

    @SneakyThrows
    @DeleteMapping("/admin/delete")
    public void delete(
            @RequestBody @Nullable final User user
    ) {
        userService.delete(user);
    }

    @SneakyThrows
    @DeleteMapping("/admin/deleteAll")
    public void deleteAll() {
        userService.deleteAll();
    }

    @SneakyThrows
    @PutMapping("/update/firstName/{id}/{newName}")
    public void updateFirstName(
            @PathVariable @Nullable final String id,
            @PathVariable @Nullable final String newName
    ) {
        @Nullable User user = userService.findById(id);
        user.setFirstName(newName);
        userService.save(user);
    }

    @SneakyThrows
    @PutMapping("/update/secondName/{id}/{newName}")
    public void updateSecondName(
            @PathVariable @Nullable final String id,
            @PathVariable @Nullable final String newName
    ) {
        @Nullable User user = userService.findById(id);
        user.setSecondName(newName);
        userService.save(user);
    }

    @SneakyThrows
    @PutMapping("/update/lastName/{id}/{newName}")
    public void updateLastName(
            @PathVariable @Nullable final String id,
            @PathVariable @Nullable final String newName
    ) {
        @Nullable User user = userService.findById(id);
        user.setLastName(newName);
        userService.save(user);
    }

}
