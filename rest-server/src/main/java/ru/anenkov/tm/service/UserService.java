package ru.anenkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import ru.anenkov.tm.exception.empty.EmptyEntityException;
import ru.anenkov.tm.exception.empty.EmptyLoginException;
import ru.anenkov.tm.exception.empty.EmptyIdException;
import ru.anenkov.tm.repository.UserRepository;
import org.springframework.stereotype.Service;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.model.User;
import lombok.SneakyThrows;

import javax.annotation.Resource;
import javax.jws.WebParam;

@Service
public class UserService {

    @Nullable
    @Autowired
    private UserRepository userRepository;

    @Nullable
    @Resource
    private AuthenticationManager authenticationManager;

    @SneakyThrows
    public boolean login(
            @Nullable @WebParam(name = "username") final String username,
            @Nullable @WebParam(name = "password") final String password
    ) {
        try {
            @Nullable final UsernamePasswordAuthenticationToken token =
                    new UsernamePasswordAuthenticationToken(username, password);
            assert authenticationManager != null;
            @NotNull final Authentication authentication =
                    authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return authentication.isAuthenticated();
        } catch (final Exception ex) {
            return false;
        }
    }

    @Nullable
    @SneakyThrows
    public User profile() {
        final SecurityContext securityContext = SecurityContextHolder.getContext();
        final Authentication authentication = securityContext.getAuthentication();
        final String username = authentication.getName();
        assert userRepository != null;
        return userRepository.findByLogin(username);
    }

    @SneakyThrows
    public boolean logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return true;
    }

    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public User findUserByFirstName(@Nullable final String firstName) {
        if (firstName == null || firstName.isEmpty()) throw new EmptyLoginException();
        return userRepository.findUserByFirstName(firstName);
    }

    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public User findUserBySecondName(@Nullable final String secondName) {
        if (secondName == null || secondName.isEmpty()) throw new EmptyLoginException();
        return userRepository.findUserBySecondName(secondName);
    }

    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public User findUserByLastName(@Nullable final String lastName) {
        if (lastName == null || lastName.isEmpty()) throw new EmptyLoginException();
        return userRepository.findUserByLastName(lastName);
    }

    @Transactional
    @SneakyThrows
    public void deleteUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        userRepository.deleteUserByLogin(login);
    }

    @Nullable
    @SneakyThrows
    @Transactional
    public User save(@Nullable final User user) {
        if (user == null) throw new EmptyEntityException();
        return userRepository.save(user);
    }

    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public User findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.findById(id).orElse(null);
    }

    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public Iterable<User> findAll() {
        return userRepository.findAll();
    }

    @SneakyThrows
    @Transactional(readOnly = true)
    public long count() {
        return userRepository.count();
    }

    @SneakyThrows
    @Transactional
    public void deleteById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        userRepository.deleteById(id);
    }

    @SneakyThrows
    @Transactional(readOnly = true)
    public void delete(@Nullable final User user) {
        if (user == null) throw new EmptyEntityException();
        userRepository.delete(user);
    }

    @SneakyThrows
    @Transactional(readOnly = true)
    public void deleteAll() {
        userRepository.deleteAll();
    }

}
