package ru.anenkov.tm.web.controller;

import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.security.test.context.support.WithUserDetails;
import ru.anenkov.tm.configuration.WebApplicationConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.anenkov.tm.configuration.ApplicationConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import ru.anenkov.tm.controller.AuthController;
import org.junit.runner.RunWith;
import org.junit.Test;

import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {WebApplicationConfiguration.class, ApplicationConfiguration.class})
@AutoConfigureMockMvc
@WithUserDetails("forTests")
public class AuthenticatedPageTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    AuthController authController;

    @Test
    public void pageLoginViewTest() throws Exception {
        assertThat(authController).isNotNull();
        this.mockMvc.perform(MockMvcRequestBuilders.get("/"))
                .andDo(print())
                .andExpect(authenticated());
    }

    @Test
    public void pageTestsViewTest() throws Exception {
        assertThat(authController).isNotNull();
        this.mockMvc.perform(MockMvcRequestBuilders.get("/tests"))
                .andDo(print())
                .andExpect(authenticated());
    }

    @Test
    public void pageProjectsViewTest() throws Exception {
        assertThat(authController).isNotNull();
        this.mockMvc.perform(MockMvcRequestBuilders.get("/projects"))
                .andDo(print())
                .andExpect(authenticated());
    }

}
